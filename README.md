rxnet-trial-proj README
=======================

This Unity project is a prototype to test the feasibility of using [System.Reactive](https://github.com/dotnet/reactive) with Unity.  It was prompted by the promising, popular, and well-designed [UniRx](https://github.com/neuecc/UniRx) library.  The only real deficit I see with UniRx is that it does not offer [all the operators](http://reactivex.io/documentation/operators.html) that System.Reactive does; see [issue 105](https://github.com/neuecc/UniRx/issues/105).  I am relatively green when it comes to reactive programming, so implementing them directly in UniRx wasn't in my wheelhouse.  However, I do have experience getting around what appears to be the major pain point, noted in [issue 324](https://github.com/neuecc/UniRx/issues/324), of using System.Reactive in Unity with a platform like iOS: Ahead-Of-Time (AOT) compilation.

## Motivation

I like UniRx and I'd love to have access to all the operators.  Perhaps this prototype points to a potential synthesis where one can take System.Reactive's library wholesale and let UniRx focus only on the special sauce that's required for Unity usage.  

## Excuses
<img src="screenshot.png" alt="Screenshot of ugly iOS app" width="25%" align="right"/>

This thing is ugly.  From its code to its UI, it ain't anything to look at.  But it works: [here's a video of it working on iOS](https://twitter.com/shanecelis/status/1144841367073558528), and it validates the idea that there's a path forward to use System.Reactive within Unity on all platforms&mdash;including AOT platforms.

The AOT code generation took a while to get working.  There is extraneous code in there that should be removed.  However, I don't know which code is extraneous.

This code does not make everything in System.Reactive work.  I expect many things don't work yet but I see no reason why applying the same techniques shouldn't be sufficient to provide support for the whole library.  

### Eliciting AOT Types

The hardest part is determining what method requires which types for AOT code generation because sometimes [Unity gives those types](https://twitter.com/shanecelis/status/1144838671818002432) and sometimes [Unity withholds those types](https://twitter.com/shanecelis/status/1144459101092454402).  

A technique I found useful when an il2cpp build did not give me a useful stack trace was to do the following: 1) Run the same project in the Unity Editor. 2) Debug it by setting a breakpoint or throwing an exception along the same code path that il2cpp build got stuck.  3) Compare your stunted il2cpp stack trace, which doesn't show the generic types, to the Editor stack trace (may work better in JetBrains Rider), which often does show the generic types.  4) Add the generic types being used on the desktop to the AOT workaround code.  5) Rebuild on il2cpp and see if the AOT workaround gets you further.  The stacktraces can look very similar so keep logs because you might be making progress and not know it.

Note: I found that [JetBrains Rider](https://www.jetbrains.com/rider/) provided the most informative stack traces on macOS and it'll hopefully make it easier to finish the AOT coverage for System.Reactive.

(It may be easier to do this work on any AOT platform.)

## Requirements

* Unity2019.1.6f1

Tested on macOS and iOS only.

## Installation

Clone the repository and get the submodule.

```sh
$ git clone https://gitlab.com/shanecelis/rxnet-trial-proj.git
$ git submodule update --init

```

## Running

Open and run like a Unity project.  The submodule that compiles "BehindTheVeil.dll" is located in the "Strathweb.Samples.CSharp.NoVisibilityChecks" folder.  It is explained in more detail in the AOT section.

```sh
$ cd Strathweb.Samples.CSharp.NoVisibilityChecks
$ cd BehindTheVeil
$ make install; # Build and copy BehindTheVeil.dll to Assets folder.
```

## TryToDoAThing.cs

This project demonstrates use of System.Reactive on both the desktop (macOS) and an AOT platform (iOS).  The class `TryToDoAThing` exercises a number of attempts, some that work and some that don't work.  

### Attempt 1 Passes

The `Range` observable works out of the box on the desktop.  However, if a subscription uses Unity's API, it must `SubscribeOn` to a specialized Unity scheduler that will run it on the main thread.

```cs
  void Attempt1Works() {
    Observable.Range(0, 10)
      .Where(i => i % 2 == 0)
      .SubscribeOn(unityScheduler)
      .Subscribe((int i) => Log($"i {i}"));
  }
```

### Attempt 2 and 3 Fail

`Timer` and `Interval` operators do not work out of the box.  `ObserveOn` with specialized scheduler is insufficient.  Workarounds follow.

```cs
  void Attempt2Fails() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }

  void Attempt3Fails() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .ObserveOn(unityScheduler)
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }
```

### Attempt 4 Works

If the `Interval` (or `Timer`) is directly given a specialized scheduler, it will work.  However, this means we'd have to pass a specialized scheduler to all timers, `SubscribeOn`, and `ObserveOn`. And in general, if I'm in Unity I want everything to run on the main thread by default. I couldn't find a good way to plug in my scheduler as a new default scheduler in System.Reactive, but I did find a medium way.

```cs
  void Attempt4Works() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f), unityScheduler)
   // .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }
```

### Attempt 5 Works

System.Reactive has a public interface IConcurrencyAbstractionLayer, but it's not pluggable that I could tell so `UnsafeInstall` uses reflection to write into a private backing field. It's definitely not ideal but it seems to make all System.Reactive's schedulers do the right thing (at a glance), namely the timers, observers, and subscriptions run on the main thread by default.

```cs
  void Attempt5Works() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();

    // Looks like a normal Observable call.
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }
```

### Attempts 6, 7, 8, 9 

Each of these attempts were variations and simplifications to try and get iOS builds working with `Timer` or `Interval`. 

## AOT Code Generation

To make System.Reactive work on iOS we must force AOT code generation as [suggested by Unity](https://docs.unity3d.com/Manual/ScriptingRestrictions.html).  Attempt 1, which has no timers, was relatively easy to enable.

```cs
  public static void FixAOT() {
    FixObservable<int>();
    throw new System.InvalidOperationException
      ("This method is used for AOT code "
        + "generation only. Do not call at runtime.");
  }

  public static void FixObservable<T>() {
    GenSchedule<
      Tuple<IScheduler,
      SingleAssignmentDisposable, // IDisposable
      IObserver<T>>>();
    throw new System.InvalidOperationException
      ("This method is used for AOT code "
        + "generation only. Do not call at runtime.");
  }

  public static void GenSchedule<T>() {
    Scheduler.Schedule<T>(null, default(T), null);

    throw new System.InvalidOperationException
      ("This method is used for AOT code "
        + "generation only. Do not call at runtime.");
  }

```

As may be apparent from the exceptions, one interesting property of the AOT code generation is that you write code that is never run. 

The other attempts to include timers were more difficult because the AOT code generation requires access to classes and types that are not visible outside of System.Reactive.  There were a couple different options: 1) One choice was to download the [System.Reactive source code](https://github.com/dotnet/reactive) and patch it directly. 2) Or I could make a small piece of AOT code generation that has visibility into System.Reactive's internals.

Option 1 isn't bad, and may be a less complicated path when all is said and done, but I wanted to avoid touching the released System.Reactive.dll if at all possible.  I choose option 2, which requires compiling code that C# compilers will generally refuse to compile.

## Hacking the Compiler

[Filip W](https://www.strathweb.com/2018/10/no-internalvisibleto-no-problem-bypassing-c-visibility-rules-with-roslyn/) wrote an article on how to run code that accesses another package's internals, which requires two things: 1) Ask the runtime not to bother with visibility checks, and 2) ask the compiler to ignore code visibility.  I used his [github repo](https://github.com/filipw/Strathweb.Samples.CSharp.NoVisibilityChecks) to only do the second part: compile code ignoring visibility concerns.  This allows me to write a small library `BehindTheVeil.dll` that _appears_ to access System.Reactive's internals but never actually runs but is sufficient to prompt the necessary AOT code generation.

A better solution would be to have the Unity AOT compiler be smarter, or have a better means of specifying AOT types, or ask System.Reactive to provide some hooks for AOT support.

Tweet Threads
=============

Here are some threads that I put together as I as was working on this.

* [Thread:](https://twitter.com/shanecelis/status/1139742558500900864) Observables are hard.

* [Thread:](https://twitter.com/shanecelis/status/1142131366336892928) Observables are fun!

* [Thread:](https://twitter.com/shanecelis/status/1143369193234018304) Need a more liberal compiler?

* [Thread:](https://twitter.com/shanecelis/status/1144393322678706176) Getting System.Reactive working on macOS and iOS
