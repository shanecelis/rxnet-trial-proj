using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reflection;

public class UnityConcurrencyAbstractionLayer : IConcurrencyAbstractionLayer
{
  public UnityScheduler unityScheduler;

  public void UnsafeInstall() {
    // https://stackoverflow.com/questions/1259222/how-to-access-internal-class-using-reflection
    var assembly = typeof(Unit).Assembly;
    var clazz = assembly.GetType("System.Reactive.Concurrency.ConcurrencyAbstractionLayer");
    var field = clazz.GetField("<Current>k__BackingField", BindingFlags.Static | BindingFlags.NonPublic);
    field.SetValue(null, this);
  }

  /// <summary>
  /// Queues a method for execution at the specified relative time.
  /// </summary>
  /// <param name="action">Method to execute.</param>
  /// <param name="state">State to pass to the method.</param>
  /// <param name="dueTime">Time to execute the method on.</param>
  /// <returns>Disposable object that can be used to stop the timer.</returns>
  public IDisposable StartTimer(Action<object> action, object state, TimeSpan dueTime)
  {
    Debug.Log("StartTimer Action " + action);
    var boolDisposable = new BooleanDisposable();
    unityScheduler.StartCoroutine(unityScheduler.WaitThen((float) dueTime.TotalSeconds, () => {
          if (! boolDisposable.IsDisposed)
            action(state);
        }));
    return boolDisposable;
  }

  /// <summary>
  /// Queues a method for periodic execution based on the specified period.
  /// </summary>
  /// <param name="action">Method to execute; should be safe for reentrancy.</param>
  /// <param name="period">Period for running the method periodically.</param>
  /// <returns>Disposable object that can be used to stop the timer.</returns>
  public IDisposable StartPeriodicTimer(Action action, TimeSpan period)
  {
    Debug.Log("StartPeriodicTimer Action " + action);
    var boolDisposable = new BooleanDisposable();
    unityScheduler
      .StartCoroutine(unityScheduler
                      .WaitPeriodic(boolDisposable, (float) period.TotalSeconds,
                                    () => {
                                      if (! boolDisposable.IsDisposed)
                                        action();
                                    }));
    return boolDisposable;
  }

  /// <summary>
  /// Queues a method for execution.
  /// </summary>
  /// <param name="action">Method to execute.</param>
  /// <param name="state">State to pass to the method.</param>
  /// <returns>Disposable object that can be used to cancel the queued method.</returns>
  public IDisposable QueueUserWorkItem(Action<object> action, object state) {
    return StartTimer(action, state, TimeSpan.Zero);
  }

  /// <summary>
  /// Blocking sleep operation.
  /// </summary>
  /// <param name="timeout">Time to sleep.</param>
  public void Sleep(TimeSpan timeout) {
    throw new Exception("We probably never want to do this.");
    // Thread.Sleep(timeout);
  }

  /// <summary>
  /// Starts a new stopwatch object.
  /// </summary>
  /// <returns>New stopwatch object; started at the time of the request.</returns>
  public IStopwatch StartStopwatch() => new UnityStopwatch();

  /// <summary>
  /// Gets whether long-running scheduling is supported.
  /// </summary>
  public bool SupportsLongRunning => false;

  /// <summary>
  /// Starts a new long-running thread.
  /// </summary>
  /// <param name="action">Method to execute.</param>
  /// <param name="state">State to pass to the method.</param>
  public void StartThread(Action<object> action, object state) {
    throw new Exception("NYI");
  }
}
