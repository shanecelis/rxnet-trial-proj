using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;

public class UnityScheduler : MonoBehaviour, IScheduler {

  public DateTimeOffset Now => DateTimeOffset.UtcNow;

  public IDisposable Schedule<T>(T state, Func<IScheduler, T, IDisposable> action) {
    // Debug.Log($"Schedule 0: {state}, {action}");
    // Debug.Log($"Schedule 0 type: {typeof(T)}");
    Debug.Log($"Schedule 0 State" + typeof(T));
    return action(this, state);
  }

  public IDisposable Schedule<T>(T state, TimeSpan dueTime, Func<IScheduler, T, IDisposable> action) {
    Debug.Log($"Schedule 1 State" + typeof(T));
    // Debug.LogException(new System.Exception("ugh"));
    // throw new System.Exception("ugh");
    // return action(this, state);
    var d = new SingleAssignmentDisposable();
    StartCoroutine(WaitThen((float) dueTime.TotalSeconds,
                            () => {
                              var d2 = action(this, state);
                              if (! d.IsDisposed)
                                d.Disposable = d2;
                              else
                                d2.Dispose();
                            }));
    return d;
  }

  public IDisposable Schedule<T>(T state, DateTimeOffset dueTime, Func<IScheduler, T, IDisposable> action) {
    Debug.Log($"Schedule 2 State" + typeof(T));
    // Debug.Log($"Schedule 2");
    return Schedule(state, dueTime - Now, action);
  }

  public IEnumerator WaitThen(float waitTime, Action action) {
    yield return new WaitForSeconds(waitTime);
    action();
  }

  public IEnumerator WaitPeriodic(ICancelable cancelable, float waitTime, Action action) {
    var wait = new WaitForSeconds(waitTime);
    while (! cancelable.IsDisposed) {
      yield return wait;
      action();
    }
  }


}
