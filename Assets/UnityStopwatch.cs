using System;
using System.Reactive.Concurrency;
using UnityEngine;

public class UnityStopwatch : IStopwatch {
  private float start;
  public UnityStopwatch() {
    start = Time.time;
  }

  public TimeSpan Elapsed => TimeSpan.FromSeconds(Time.time - start);
}
