﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
// using UniRx;
// using UniRx.Triggers;

public class TryToDoAThing : MonoBehaviour {
  // public ObservablePointerDownTrigger trigger;
  public UnityScheduler unityScheduler;
  public Text log;
  public InputField numberField;
  // public int whichAttempt = 1;

  /** Just vanilla System.Reactive and a few dependency libraries and it
      WORKS!

      Required netstandard2.0 Libraries:
      System.Reactive,
      System.Runtime.CompilerServices.Unsafe,
      System.Threading.Tasks.Extensions

      Exercised on Unity2019.1.6f1.
  */
  void Attempt1Works() {
    Observable.Range(0, 10)
      .Where(i => i % 2 == 0)
      .SubscribeOn(unityScheduler)
      .Subscribe((int i) => Log($"i {i}"));
  }

  /** Does not work with Interval. */
  void Attempt2Fails() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }

  /** Does not work with Interval with a custom scheduler. */
  void Attempt3Fails() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .ObserveOn(unityScheduler)
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }

  /** Works if the Interval is directly given a custom scheduler. */
  void Attempt4Works() {
    Observable.Interval(TimeSpan.FromSeconds(0.5f), unityScheduler)
   // .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }

  /** Works if we take over the ConcurrencyAbstractionLayer.

      We don't need to specify schedulers to timers or on what thread the
      subscriptions should run.

      Does not work on iOS yet because of AOT. There is a trick to make the
      compiler generate the required AOT code. You just reference the generic
      class or method with the type that you will eventually need at runtime.
      This dead code never runs. It might look like this:

      IEnumerable<long> x = null;
      IObservable<char> y = null;
      GenericClass<int> z = null;
      z.Method<string>(null, null);
      throw new System.InvalidOperationException("DO NOT RUN. This is for AOT code generation only.");

      Unfortunately, many of the classes and types I need to generate are
      internal to System.Reactive, but I found a crazy hack that might help.
      Someone wrote the equivalent of "#define private public" but for C#.
      There's a little known attribute that you can use to disable the runtime
      from checking for visibility access. That gets the runtime to let you play
      behind the veil; however, the standard C# compiler isn't going to let you
      call methods you can't see. So we make use of the Roslyn compiler that'll
      write to whatever methods we like regardless of visibility.

      My hope is that we can break visibility rules just enough to generate the
      right AOT code. And it's not as bad of a sin since we're never calling it
      at runtime.


   */


  /** Works if we take over the ConcurrencyAbstractionLayer.

      We don't need to specify schedulers to timers or on what thread the
      subscriptions should run.
  */
  void Attempt5Works() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();

    // Looks like a normal Observable call.
    Observable.Interval(TimeSpan.FromSeconds(0.5f))
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"));
  }


  /*NotSupportedException: IL2CPP encountered a managed type which it cannot convert ahead-of-time. The type uses generic or array types which are nested beyond the maximum depth which can be converted.
    at System.Reactive.Concurrency.Scheduler.ScheduleAction[TState] (System.Reactive.Concurrency.IScheduler scheduler, TState state, System.Action`1[T] action) [0x00000] in <00000000000000000000000000000000>:0
    at System.Reactive.Producer`2[TTarget,TSink].SubscribeRaw (System.IObserver`1[T] observer, System.Boolean enableSafeguard) [0x00000] in <00000000000000000000000000000000>:0
    at TryToDoAThing.RunAttempt (System.Int32 whichAttempt) [0x00000] in <00000000000000000000000000000000>:0
    at TryToDoAThing.RunButton () [0x00000] in <00000000000000000000000000000000>:0

    (Filename: currently not available on il2cpp Line: -1)
  */

  void Attempt6Works() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();

    // Looks like a normal Observable call.
    Observable.Timer(TimeSpan.FromSeconds(1f))
      .Subscribe(_ => Log($"did it"));
  }

  void Attempt7() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();
    /*NotSupportedException: IL2CPP encountered a managed type which it cannot convert ahead-of-time. The type uses generic or array types which are nested beyond the maximum depth which can be converted.
      at System.Reactive.Concurrency.Scheduler.ScheduleAction[TState] (System.Reactive.Concurrency.IScheduler scheduler, TState state, System.Action`1[T] action) [0x00000] in <00000000000000000000000000000000>:0
      at System.Reactive.Producer`2[TTarget,TSink].SubscribeRaw (System.IObserver`1[T] observer, System.Boolean enableSafeguard) [0x00000] in <00000000000000000000000000000000>:0
      at TryToDoAThing.RunAttempt (System.Int32 whichAttempt) [0x00000] in <00000000000000000000000000000000>:0
      at TryToDoAThing.RunButton () [0x00000] in <00000000000000000000000000000000>:0

      (Filename: currently not available on il2cpp Line: -1)
      */

    // Looks like a normal Observable call.
    Observable.Interval(TimeSpan.FromSeconds(0.5f)).Take(0)
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"),
                 () => Log("Complete"));
  }

  void Attempt8() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();

    Observable.Interval(TimeSpan.FromSeconds(0.5f)).Take(1)
      .Where(i => i % 2 == 0)
      .Subscribe((long i) => Log($"i {i}"),
                 () => Log("Complete"));
  }

  void Attempt9() {
    var ucal = new UnityConcurrencyAbstractionLayer {
      unityScheduler = unityScheduler
    };
    ucal.UnsafeInstall();

    var o = Observable.Interval(TimeSpan.FromSeconds(0.5f)).Take(1);
    // Just make the observable don't even run it!
  }

  void Start() {
    // Log("Attempt " + whichAttempt);
    // RunAttempt(whichAttempt);
    RunButton();
  }

  public void RunButton() {
    int attempt = 0;
    if (int.TryParse(numberField.text, out attempt)) {
      RunAttempt(attempt);
    } else {
      Log($"Cannot parse {numberField.text} as integer.");
    }
  }

  public void ClearLog() {
    log.text = "";
  }

  public void RunAttempt(int whichAttempt) {
    Log("Attempt " + whichAttempt);
    switch (whichAttempt) {
      case 1:
        Attempt1Works();
        break;
      case 2:
        Attempt2Fails();
        break;
      case 3:
        Attempt3Fails();
        break;
      case 4:
        Attempt4Works();
        break;
      case 5:
        Attempt5Works();
        break;
      case 6:
        Attempt6Works();
        break;
      case 7:
        Attempt7();
        break;
      case 8:
        Attempt8();
        break;
      case 9:
        Attempt9();
        break;
      default:
        Log("No such attempt " + whichAttempt);
        break;
    }
    // var range = Observable.Range(0, 10)
    //   .Where(i => i % 2 == 0)
    //   .RepeatWhen(end => end.Delay(TimeSpan.FromSeconds(2f)).Take(2))
    //   .Subscribe((int i) => Debug.Log($"i {i}"),
    //              () => Debug.Log("Complete"));
    // var closer = new Subject<Unit>();
    // var ucal = new UnityConcurrencyAbstractionLayer {
    //   unityScheduler = unityScheduler
    // };
    // ucal.UnsafeInstall();

    // Observable.Range(0, 10)
    // var d = Observable.Interval(TimeSpan.FromSeconds(0.5f), unityScheduler)
    // var d = Observable.Interval(TimeSpan.FromSeconds(0.5f))
    //   // .ObserveOn(unityScheduler)
    //   .Take(3)
    //   // .ObserveOn(Scheduler.NewThread)
    //   // .SubscribeOn(Scheduler.CurrentThread)
    //   // .SubscribeOn(unityScheduler)
    //   .Subscribe((i) => Log($"i {i}"));
    // yield return new WaitForSeconds(3f);
    // d.Dispose();

    // var mainThread = Thread.CurrentThread;//.ManagedThreadId;
    // var range =
    //   // Observable.Range(0, 10)
    //   Observable.Interval(TimeSpan.FromSeconds(0.5f)).Take(10)

    //   .RepeatWhen(end => end.Delay(TimeSpan.FromSeconds(2f)).Take(2))
    //   .Window(() => Observable.Timer(TimeSpan.FromSeconds(1.5f)))

    //   // .ObserveOn(Scheduler.CurrentThread)
    //   .SubscribeOn(Scheduler.CurrentThread)
    //   // .SubscribeOn(mainThread)
    //   // .CompleteOn(Scheduler.CurrentThread)
    //   .Subscribe(window =>
    //       {
    //         Log("Window");
    //         window

    //         .SubscribeOn(Scheduler.CurrentThread)
    //         .Subscribe((i) => Log($"i {i}"),
    //                    () => Log("window Complete"));
    //         // .Subscribe((long i) => Log($"i {i}"));
    //       });
    //       //, () => Log("Complete"));

      // Observable.Range(0, 10)
      // // .RepeatWhen(end => end.Delay(TimeSpan.FromSeconds(2f)).Take(2))
      // // .Window(() => Observable.Timer(TimeSpan.FromSeconds(1.5f)))

      // .ObserveOn(Scheduler.CurrentThread)
      // .SubscribeOn(Scheduler.CurrentThread)
      // // .CompleteOn(Scheduler.CurrentThread)
      // .SubscribeOn(Scheduler.CurrentThread)
      // .Subscribe((int i) => Log($"i {i}"),
      //            () => Log("window Complete"));
    //, () => Log("Complete"));


    // trigger.OnPointerDownAsObservable()
    //   .SubscribeOn(Scheduler.CurrentThread)
    //   .Subscribe(eventData => Log("Pointer down"));
  }

  int count = 0;
  void Log(string s) {
    count++;
    // if (count % 5 == 0)
    //   log.text = "";
    log.text += s + "\n";
    Debug.Log(s);
  }

}

    // https://github.com/modesttree/Zenject/issues/219
    // https://docs.unity3d.com/560/Documentation/Manual/ScriptingRestrictions.html
    /*
      In order to work in an Ahead Of Time (AOT) Compilation, we have to specify
      which types we will be instantiating.
    */
    public static class _AOTFixer {

      public static void FixAOT() {
        FixObservable<PointerEventData>();
        FixObservable<int>();
        FixObservable<long>();
        FixObservable<object>();
        BehindTheVeil._AOTFixer.Case0<int>();
        BehindTheVeil._AOTFixer.Case1<long>();

        BehindTheVeil._AOTFixer.GenScheduleAction();
        // FixObservable<IObservable<int>>();

        throw new System.InvalidOperationException
          ("This method is used for AOT code "
           + "generation only. Do not call at runtime.");
      }

      public static void GenScheduleAction<T>() {
        // var timeSpan = TimeSpan.Zero;
        // var dateTimeOffset = DateTimeOffset.UtcNow;
        // IScheduler ischeduler = null;
        // // Scheduler
        // //   .Schedule<T>(default(T), null);
        // ischeduler
        //   .Schedule<T>(default(T), null);

        // ischeduler
        //   .Schedule<T>(default(T), timeSpan, null);

        // ischeduler
        //   .Schedule<T>(default(T), dateTimeOffset, null);

        // Scheduler
        //   .Schedule<T>(null, default(T), null);
        BehindTheVeil._AOTFixer.GenScheduleAction<T>();
      }

#if VALIDATE_HACK
      public static void GenSchedule<T>() {
        BehindTheVeil._AOTFixer.GenSchedule<T>();
      }
#else
      /** These are public methods, so we can do the AOT generation here. But I
          wanted to ensure that calling a DLL in another assembly with the same
          content would do the same work, and fortunately it does! */
      public static void GenSchedule<T>() {
        var timeSpan = TimeSpan.Zero;
        var dateTimeOffset = DateTimeOffset.UtcNow;
        IScheduler ischeduler = null;
        // Scheduler
        //   .Schedule<T>(default(T), null);
        ischeduler
          .Schedule<T>(default(T), null);

        ischeduler
          .Schedule<T>(default(T), timeSpan, null);

        ischeduler
          .Schedule<T>(default(T), dateTimeOffset, null);

        Scheduler
          .Schedule<T>(null, default(T), null);
      }
#endif

      public static void FixObservable<T>() {
        GenSchedule<
          Tuple<IScheduler,
          // IDisposable,
          SingleAssignmentDisposable,
          IObserver<T>>>();

        GenSchedule<
          Tuple<IScheduler,
          // IDisposable>>();
          SingleAssignmentDisposable>>();

        GenSchedule<
          Tuple<IScheduler,
          T>>();

        GenScheduleAction<T>();

        throw new System.InvalidOperationException
          ("This method is used for AOT code "
           + "generation only. Do not call at runtime.");
      }


    }
